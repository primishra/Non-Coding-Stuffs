## Classical Mechanics & Hydro Mechanics

- [https://drive.google.com/drive/folders/1qOJ-xbGY98x0NgOZJ8GAN-oYyqI-7A-b?usp=sharing](https://drive.google.com/drive/folders/1qOJ-xbGY98x0NgOZJ8GAN-oYyqI-7A-b?usp=sharing)

## Deep Learning (IIT Kgp) Notes

- [https://drive.google.com/drive/folders/1Tm0XNBSSdbg0nOWnMAwMWChSepJyOVh2?usp=sharing](https://drive.google.com/drive/folders/1Tm0XNBSSdbg0nOWnMAwMWChSepJyOVh2?usp=sharing)

## Differntial Geometry & Reimann Geometry

- [https://drive.google.com/drive/folders/1kwcbEjX6Jhacj6XbYfCTAYarOMUpF24o?usp=sharing](https://drive.google.com/drive/folders/1kwcbEjX6Jhacj6XbYfCTAYarOMUpF24o?usp=sharing)

## Discrete Mathematics

- [https://drive.google.com/drive/folders/1U7bm4wMhrVdT6S6MM3ULRCNU9xsRpgl1?usp=sharing](https://drive.google.com/drive/folders/1U7bm4wMhrVdT6S6MM3ULRCNU9xsRpgl1?usp=sharing)

## Integral Transforms and Integral Equations

- [https://drive.google.com/drive/folders/1MzGYYlofZ88mfznikv5LIP2zP6iKiIA4?usp=sharing](https://drive.google.com/drive/folders/1MzGYYlofZ88mfznikv5LIP2zP6iKiIA4?usp=sharing)

## Machine Learning (IIT Kgp) Notes

- [https://drive.google.com/drive/folders/1LJaRAbKE5KpDJoEYPaPyXpwXZePV5UUF?usp=sharing](https://drive.google.com/drive/folders/1LJaRAbKE5KpDJoEYPaPyXpwXZePV5UUF?usp=sharing)

## Number Theory

- [https://drive.google.com/drive/folders/1KCAF939g8yqcGbeFVVQXM6JTf7hR1jlE?usp=sharing](https://drive.google.com/drive/folders/1KCAF939g8yqcGbeFVVQXM6JTf7hR1jlE?usp=sharing)

## Operations Research

- [https://drive.google.com/drive/folders/1Vh0oKKbZov1kHwpiGSo50VVSmfpZOgBh?usp=sharing](https://drive.google.com/drive/folders/1Vh0oKKbZov1kHwpiGSo50VVSmfpZOgBh?usp=sharing)

## Ordinary Differential Equations with Special Functions

- [https://drive.google.com/drive/folders/1eoFbEP-XkVVpeEnWIhcR2wtDrdNXSLLw?usp=sharing](https://drive.google.com/drive/folders/1eoFbEP-XkVVpeEnWIhcR2wtDrdNXSLLw?usp=sharing)

## Reinforcement Learning (IIT Kgp) Notes

- [https://drive.google.com/drive/folders/1V-ecq9DW8t1AowntY907sHkYE-BwxxMN?usp=sharing](https://drive.google.com/drive/folders/1V-ecq9DW8t1AowntY907sHkYE-BwxxMN?usp=sharing)

## Statistical Inference (BIT Mesra) Notes

- [https://drive.google.com/drive/folders/1QHdbfJmdAd7srednWdTVJLoVmLTUxdMX?usp=sharing](https://drive.google.com/drive/folders/1QHdbfJmdAd7srednWdTVJLoVmLTUxdMX?usp=sharing)

## Statistical Inference (IIT Bombay) Notes

- [https://drive.google.com/drive/folders/1afCBCKbupqwFEsgs0bdlBukj58xte3kJ?usp=sharing](https://drive.google.com/drive/folders/1afCBCKbupqwFEsgs0bdlBukj58xte3kJ?usp=sharing)


#### To be added :
* M1
* M2
* M4
* DM
* DACA
* NM
* P&S
